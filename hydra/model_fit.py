import pickle

import pandas as pd
from omegaconf import DictConfig
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from xgboost import XGBClassifier

import hydra


@hydra.main(version_base=None, config_path="./", config_name="model_config.yaml")
def main(cfg: DictConfig) -> None:
    # Загрузка датасета
    iris = pd.read_csv("./preproc_data.csv")
    model_type = cfg.model

    if model_type == "dtr":
        X = iris[["sepal_length", "sepal_width", "petal_length", "petal_width"]]
        y = iris[["species"]]
        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.2, random_state=42
        )
        # читаем из конфига настройки для обучения модели
        max_depth = cfg.dtr.max_depth
        model = DecisionTreeClassifier(max_depth=max_depth)
        model.fit(X_train, y_train)

    elif model_type == "xgboost":
        X = iris[["sepal_length", "sepal_width", "petal_length", "petal_width"]]
        y0 = iris[["species"]]
        y = pd.get_dummies(y0, prefix="class")
        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.2, random_state=42
        )
        # читаем из конфига настройки для обучения модели
        n_estimators = cfg.xgboost.n_estimators
        model = XGBClassifier(n_estimators=n_estimators)
        model.fit(X_train, y_train)

    with open("model.pickle", "wb") as f:
        pickle.dump(model, f)


if __name__ == "__main__":
    main()
