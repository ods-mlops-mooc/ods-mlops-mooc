import pandas as pd
from omegaconf import DictConfig

import hydra


@hydra.main(version_base=None, config_path="./", config_name="preproc_config.yaml")
def main(cfg: DictConfig) -> None:
    df = pd.read_csv("iris_dataset.csv")
    preproc_type = cfg.preproc_type

    if preproc_type == "use_first_rows":
        n = cfg.use_first_rows.first_n_rows
        df_preproc = df.head(n)

    elif preproc_type == "use_last_rows":
        n = cfg.use_last_rows.last_n_rows
        df_preproc = df.tail(n)

    df_preproc.to_csv("preproc_data.csv")


if __name__ == "__main__":
    main()
