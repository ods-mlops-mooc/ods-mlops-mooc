
You can clone this repo to you and then build and run an image.
You can run jupyter notebook in docker-container and write the code just right there.

1. Clone this repo to you
2. In terminal at the repo folder run command "docker build . -t mlops-course-project".
3. In terminal run command "docker run -it -p 8888:8888 mlops-course-project"
4. In the same terminal run command "poetry shell"
5. In the same terminal run command "jupyter notebook --ip=0.0.0.0 --port=8888 --allow-root --no-browser"
6. In the same terminal find an URL like http://127.0.0.1:8888/tree?token=9f0sdfdkslfjdksfjdsifjdoisjfdl and open it in your browser
7. Enjoy coding in Jupyter notebook
8. Before commit use "ruff check" and if need "ruff check --fix".
