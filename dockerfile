FROM python:3.12

WORKDIR /app

ENV POETRY_VERSION=1.8.2

RUN pip install "poetry==$POETRY_VERSION"

COPY pyproject.toml poetry.lock ./

COPY README.md ./

RUN poetry config virtualenvs.create false
RUN poetry install --with dev
RUN poetry run kaggle datasets download -d new-york-city/ny-2015-street-tree-census-tree-data
RUN poetry run ls

# COPY . /app
