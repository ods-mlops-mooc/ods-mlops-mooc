import seaborn as sns

df = sns.load_dataset("iris")
df.to_csv("iris_dataset.csv", index=False)
