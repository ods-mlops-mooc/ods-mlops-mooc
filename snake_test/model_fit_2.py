import pickle

import pandas as pd
from sklearn.model_selection import train_test_split
from xgboost import XGBClassifier

# Загрузка датасета
iris = pd.read_csv("./preproc_data_2.csv")
print(iris.columns)
X = iris[["sepal_length", "sepal_width", "petal_length", "petal_width"]]
y0 = iris[["species"]]
y = pd.get_dummies(y0, prefix="class")

# Разделение данных на обучающий и тестовый наборы
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=42
)


# Создание и обучение модели XGBoost
model = XGBClassifier()
model.fit(X_train, y_train)

with open("model_2.pickle", "wb") as f:
    pickle.dump(model, f)

# # Прогнозирование меток классов на тестовом наборе
# y_pred = model.predict(X_test)

# # Оценка точности модели
# accuracy = accuracy_score(y_test, y_pred)
# print("Accuracy:", accuracy)
